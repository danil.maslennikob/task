import time

el = 0
data = [] #  Список для хранения элементов

while True:
    try:
        el = input()
        if el.startswith('```') or el == "":  # Если начинается с '```' или пустой сторки
            continue
        data.append(el)
    except EOFError: # Окончание даннных в источнике
        break


for i in data:
	# Меняем "#" на пустую строку 
    if i.startswith("#"):
        i = ''
        print('\033c', end="" )  # Очищение терминала; 'end' предотвращает введение новых строк
        time.sleep(0.8)  # Задержка слайдов в 0,8с
    
    print(f"\033[35;1m {i}")  # Выводи gif фиолетовым цветом и интенсивным цветом
