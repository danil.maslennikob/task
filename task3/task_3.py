from random import randint, uniform
from Saxpy import saxpy
from time import time

scalar_a = 0
type_vector = input('Выберите тип вектора int/flt:  ')

try:
    scalar_a = int(input('Введите скаляр a: '))
except ValueError:
        print('Error')
        exit()


n = 100000  #  Начальное кол-во элементов вектора
step = 100000  # Шаг
stop = 1000000  

vector_x = vector_y = 0
file = open('result3.txt', 'w')

while n <= stop:

    if type_vector == 'int':
        vector_x = [randint(0, 10) for i in range(n)]
        vector_y = [randint(0, 10) for i in range(n)]
    elif type_vector == 'flt':
        vector_x = [uniform(0, 10) for flt in range(n)]
        vector_y = [uniform(0, 10) for flt in range(n)]
    else:
        exit('Вы ввели неверное значение')

    time_data = []  # список для хранения значения занимаемого времени
    averages = []  # список для хранения среднего значения занимаемого времени


    for i in range(5):

        start_time = time()  # Время начала алгоритма
        print(f'z = {saxpy(scalar_a, vector_x, vector_y)}')  # выводит на экран конечный результат
        stop_time = time()   # Время остановки алгоритма
        time_result = stop_time - start_time  # Количество времени потраченного на алгоритм
        time_data.append(time_result)
    averages.append(sum(time_data) / 5)
    file.write(f'{str(averages[-1]).replace(".", ",")}\n')  # Запись количества времени потраченного на алгоритм в файл

    n += step

file.close()
