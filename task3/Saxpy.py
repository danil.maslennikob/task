def saxpy(scalar_a, vector_x, vector_y):
    vector_z = [x * scalar_a + y for x, y in zip(vector_x, vector_y)]
    return vector_z
