from matrix import craft_matrix, matrix_multiplication
from time import time

# выбираем тип матрицы
type_of_matrix = input('Выберите тип матрицы: 1 - целочисленный, 2 - вещественный >>> ')
if type_of_matrix not in ['1', '2']:  # если тип матрицы не 1 или 2, программа закроется
    print('Выберите 1 или 2...')
    exit()

# определяем размерность матрицы

size = 50   # начальная размерность
step = 50   # шаг
stop = 500  # конечная размерность

file = open('task4.txt', 'w')

while size <= stop:

    matrix1 = craft_matrix(type_of_matrix, size)  # создается матрица 1
    matrix2 = craft_matrix(type_of_matrix, size)  # создается матрица 2

    time_data = []  # список для хранения значения занимаемого времени
    averages = []  # список для хранения среднего значения занимаемого времени

    for i in range(3):
        start_time = time()  # Время начала алгоритма
        matrix_result = matrix_multiplication(matrix1, matrix2)  # умножаем матрицы друг на друг
        stop_time = time()  # Время остановки алгоритма

        time_result = stop_time - start_time  # Количество времени потраченного на алгоритм
        time_data.append(time_result)
        print(f'для матрицы с размерностью {size} время заняло {time_result}...')
    averages.append(sum(time_data) / 3)
    file.write(f'{str(averages[-1]).replace(".", ",")}\n')  # Запись количества времени потраченного на алгоритм в файл

    size += step  # увеличиваем размерность матрицы

file.close()
