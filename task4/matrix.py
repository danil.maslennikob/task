from random import randint, uniform


def craft_matrix(type_, size):
    # создание матрицы, короторая зависит от размерности size c входящими в нее элементами от 0 до 9
    if type_ == '1':
        return [[randint(0, 9) for element in range(size)] for j in range(size)]
    elif type_ == '2':
        return [[uniform(0, 9) for element in range(size)] for j in range(size)]


def matrix_multiplication(matrix1, matrix2):
    # массив из строк 1 матрицы и столбцов 2 c 0 элементами в матрице
    z = [[0 for e in range(len(matrix1))] for r in range(len(matrix2[0]))]
    # алгоритм умнажения матриц
    for i in range(len(matrix1)):
        for j in range(len(matrix2[0])):
            for k in range(len(matrix2)):
                          z[i][j] += matrix1[i][k] * matrix2[k][j] # прибавление к нулю элементов умножения матрицы
    return z
