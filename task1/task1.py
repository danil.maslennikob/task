x = 0
total = 42  # Начальна сумма

while True:
    # Проверка вводимых значений
    try:
        x = float(input())
    except ValueError:
        continue
    except EOFError:
        print('Конец')
        break
     # Проверка деления на 0
    try:
        total += 1 / x
    except ZeroDivisionError:
        continue

print(f'Сумма ряда = {total}')  # Конечная сумма
